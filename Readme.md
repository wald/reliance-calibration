# Reliance calibration
July 2023

This directory features the (hopefully final) calibration attempt for the reliance project.

## Usage
``. setup.sh''
``python3 calibrate_adc_to_kohm.py -p data/20230724''\


## Structure
``share/'': -> python script
``data/'' : -> The raw data. 
``calib/'': -> The calibration constants in .txt format

