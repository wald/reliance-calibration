#!/usr/bin/env python

import os
import ROOT
import argparse
import datetime

ROOT.gROOT.SetBatch(False) # no windows popping up

# styling options
ROOT.gROOT.SetStyle("ATLAS")
ROOT.gStyle.SetNdivisions(505)

# old calib: ADC to Ohm
old_calib={
	23:[704,1.04,43.11e-5],
	1:[704,0.69,19.16e-5],
	2:[704,0.35, 4.79e-5],
	4:[  0,0.38, 0.59e-5],
	8:[-29,0.20, 0.12e-5],
	16:[-34,0.10, 0.03e-5],
}  
# new calib: ADC to kOhm
new_calib={
	23:[-7e-3,25.4e-4,5.0e-8,2.6e-11],
	1:[-1e-3,1.579e-3,14.83e-8,-1.462e-11,0.099e-14],
 	2:[-8e-3,84.9e-5,5.5e-9,9.8e-13],
 	4:[-4e-3,40.8e-5,53.5e-10],
 	8:[-6e-3,205e-6,12.4e-10],
 	16:[-10e-3,108e-6], 
}

def calib_to_tf1(gain,calib,tag):
	# create the tf1
	tf1=ROOT.TF1("calib_%i_%s"%(gain,tag),"pol%i"%(len(calib)-1),0,32000)
	for i,cc in enumerate(calib):
		print("calib_%i_%s: param=%i, value=%e"%(gain,tag,i,cc))
		if tag=="old": cc=cc/1000.
		tf1.SetParameter(i,cc)
		pass
	#return the tf1
	return tf1

def calib_funcs(gain,new_calib,old_calib):
	funcs={}
	funcs["new"]=calib_to_tf1(gain,new_calib[gain],"new")
	funcs["old"]=calib_to_tf1(gain,old_calib[gain],"old")
	funcs["old"].SetLineColor(ROOT.kRed)
	funcs["new"].SetLineColor(ROOT.kBlue)
	funcs["dif"]=ROOT.TF1("dif_%i"%(gain),"calib_%i_%s-calib_%i_%s"%(gain,"new",gain,"old"),0,32000)
	for i in range(len(new_calib[gain])): funcs["dif"].SetParameter(i,new_calib[gain][i])
	for i in range(len(old_calib[gain])): funcs["dif"].SetParameter(i+len(new_calib[gain]),old_calib[gain][i]/1000)
	for i in range(len(new_calib[gain])+len(old_calib[gain])):
		print("param= %i, value=%f" % (i,funcs["dif"].GetParameter(i)))
		
	return funcs

# main starts here

# pass the file name and the gain of the meaurement
parser=argparse.ArgumentParser()
args=parser.parse_args()

gains=[23,1,2,4,8,16]
mem=[]

# init drawing the plot 
c1=ROOT.TCanvas("c1","compare_calib", 400*len(gains), 400*2)
c1.Divide(len(gains),2)
tt=ROOT.TLatex()

# iterate through gains	
for i, gain in enumerate(gains):
	print("Process gain: %i" %gain)
	# switch Canvas 
	c1.cd(i+1)
	
	# create the functions
	funcs=calib_funcs(gain,new_calib,old_calib)

	# draw on canvas
	funcs["new"].Draw("")
	funcs["old"].Draw("SAME")
	funcs["new"].GetHistogram().GetXaxis().SetTitle("ADC")
	funcs["new"].GetHistogram().GetYaxis().SetTitle("Resistance [k#Omega]")
	
	tt.DrawLatexNDC(0.5,0.86,"Gain: %i" %gain)
	lg1=ROOT.TLegend(0.2,0.6,0.5,0.8)
	lg1.AddEntry(funcs["new"],"new","l")
	lg1.AddEntry(funcs["old"],"old","l")
	lg1.Draw()
	
	# store the functions
	mem.append(funcs["new"])
	mem.append(funcs["old"])
	mem.append(funcs["dif"])
	mem.append(lg1)
	
	
	c1.cd(i+len(gains)+1)
	funcs["dif"].Draw()
	print("Dif: %s"%funcs["dif"].GetExpFormula())
	funcs["dif"].GetHistogram().GetXaxis().SetTitle("ADC")
	funcs["dif"].GetHistogram().GetYaxis().SetTitle("Resistance New-Old [k#Omega]")
	
	tt.DrawLatexNDC(0.5,0.86,"Gain: %i" %gain)
	
	
	pass

# update the plotting
c1.Update()
c1.SaveAs("compare_calib.pdf")
c1.SaveAs("compare_calib.C")

print("Have a nice day!")
