#!/usr/bin/env python

import os
import ROOT
import argparse
import datetime

ROOT.gROOT.SetBatch(True) # no windows popping up

# styling options
ROOT.gROOT.SetStyle("ATLAS")
ROOT.gStyle.SetNdivisions(505)

def get_gain(path):
	""" extract the gain from the filename 
		return 0 if no gain matches
	"""
	matches={
		"1.txt":1,
		"2.txt":2,
		"4.txt":4,
		"8.txt":8,
		"16.txt":16,
		"23.txt":23,
		}
	for match in matches:
		if match in path: return matches[match]
		pass
	return 0
	
def load_file(path, gain):
	""" read data from file <path> into a TGraph """

	print("Loading file %s"%path)

	# create the graph
	g1=ROOT.TGraphErrors()
	g1.SetMarkerStyle(20) #  dot
	g1.SetNameTitle("gResVsAdc_%i"%gain,";ADC counts; Resistance [k#Omega]")
	
	# add the origin
	# g1.SetPoint(g1.GetN(),0,0)
	# g1.SetPointError(g1.GetN()-1, 0, 0)

	# read in the file
	with open(path) as f:
		for i , line in enumerate(f):
			# skip the first line
			if i == 0: continue
			chk = line.strip().split(",") # Resistance [kO],ADC

			# data
			res_kO = float(chk[0])
			err_res_kO = float(chk[1])
			adc = float(chk[2])
			err_adc =  1 #float(chk[3])
			g1.SetPoint(g1.GetN(), adc, res_kO)
			g1.SetPointError(g1.GetN()-1, err_adc, err_res_kO)
			pass
	
	# return the plot
	return g1

def calibrate(graph, gain, min_degree, max_degree, path):
	""" """

	# draw the points
	if not os.path.exists("calib/%s"%path): os.makedirs("calib/%s"%path)
	fw=open("calib/%s/calib_%s.txt"%(path,gain),"w")
	s="# Calibration: gain %i on %s"%(gain,datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
	print(s)
	fw.write(s+"\n")
	# fit polynomials
	f1s=[]
	
	scales={0:0.001,1:1,2:1e5,3:1e8,4:1e11,5:1e16}

	# Fit polynomials of order degree
	for i in range(min_degree,max_degree+1):

		# init fit function
		f1=ROOT.TF1("f%i_pol%i"%(gain,i),"pol%i"%i)
		f1.SetLineColor(i)	

		# fit
		g1.Fit(f1,"WQN") # Prefit
		g1.Fit(f1,"FM+") # Effective Variance Method

		# get the result, save it and print it out
		s="POL %i, Chi2: %.5f, NDF: %i, Chi2/NDF: %.2e"%(i,f1.GetChisquare(),f1.GetNDF(),f1.GetChisquare()/f1.GetNDF())
		print(s)
		fw.write(s+"\n")

		# also store it in the title for the legend
		f1.SetTitle("Gain %i, Pol %i, #chi^{2}_{red} %.1f" % (gain,i, f1.GetChisquare()/f1.GetNDF()))
		#f1.SetTitle("Gain %i, Pol %i" % (gain,i))
		f1s.append(f1)

		# print the parameters
		s="P %10s %16s %15s" % ("param","error","rel")
		print(s)
		fw.write(s+"\n")
		for o in range(i+1):
			scale=scales[o]*1000
			s="%i %10.3f %.0e %10.3f %.0e %10.2f" % (o,f1.GetParameter(o)*scale,1/scale,f1.GetParError(o)*scale,1/scale,abs(f1.GetParError(o)/f1.GetParameter(o)))
			print(s)
			fw.write(s+"\n")
			pass
		pass
	#close the output file
	fw.close()
	
	# return the functions
	return f1s

def draw_legend(f1s):
	""" add a legend to the graph"""
	tl=ROOT.TLegend(0.18,0.65,0.60,0.90)
	
	# set the style
	tl.SetFillColor(0)
	tl.SetLineWidth(0)
	tl.SetBorderSize(1)
	tl.SetLineColor(ROOT.kWhite)

	# add the functions to the legend
	for f1 in f1s: tl.AddEntry(f1,f1.GetTitle(),"l")
	
	# draw the legend
	# tl.Draw()
	return tl


# main starts here

# pass the file name and the gain of the meaurement
parser=argparse.ArgumentParser()
parser.add_argument("-p","--path",help="Path to input files. Example data/", required=True)
parser.add_argument("-t","--tag",help="Force the output tag, else try to extract from path")
args=parser.parse_args()

# calibration options: specify degrees of fit
opts={
	23:{"min_degree":2,"max_degree":4},
	1:{"min_degree":2,"max_degree":5}, #
	2:{"min_degree":2,"max_degree":5}, #
	4:{"min_degree":1,"max_degree":3},
	8:{"min_degree":1,"max_degree":3},
	16:{"min_degree":1,"max_degree":3}, # review
	}

# read the input files
frs={}
for f in os.listdir(args.path):
	print(f'Reading {f}')
	if not ".txt" in f: continue
	path=os.path.join(args.path, f)
	frs[path]={}
	pass
	
if not args.tag:
	for pp in args.path.split(os.sep):
		if len(pp)>=8: args.tag=pp
		pass
	pass
	

# init drawing the plot 
c1=ROOT.TCanvas("c1","calibration %s"%args.path, 400*len(frs), 400)
c1.Divide(len(frs),1)
ct=ROOT.TCanvas("ct","calibration %s"%args.path, 400, 400)

# iterate through all files = gains	
for i, path in enumerate(frs):
	# switch Canvas 
	c1.cd(i+1)
	i+=1

	# find gain from filename and load graph
	gain=get_gain(path)
	g1=load_file(path,gain)

	# draw the graph
	g1.Draw("AP")

	# fit the data and add a legend
	f1s = calibrate(g1,gain,opts[gain]["min_degree"],opts[gain]["max_degree"], args.tag)	
	tl = draw_legend(f1s)

	# store graph and the fit functions
	frs[path]["g1"]=g1
	frs[path]["f1s"]=f1s
	frs[path]["tl"]=tl

	# draw the fits
	ct.cd()
	g1.Draw("AP")
	tl.Draw()
	ct.SaveAs("plots/%s/calib_%i.pdf"%(args.tag,gain))

	#input("press any key to continue!")
	pass

# update the plotting
c1.Update()

# save the plot
if not os.path.exists("plots/%s"%args.tag): os.makedirs("plots/%s"%args.tag)
c1.SaveAs("plots/%s/calib.pdf"%args.tag)
c1.SaveAs("plots/%s/calib.png"%args.tag)
c1.SaveAs("plots/%s/calib.C"%args.tag)

# close the plot by pressing a key
#input("press any key to close the plot!")

print("Have a nice day!")
